/*
QUIZ

1. How do you create arrays in JS?
using a bracket []
For example 
name = ["a","b","c"]


2. How do you access the first character of an array?
index 0

3. How do you access the last character of an array?-1
4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
 indexOf()
5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()
6. What array method creates a new array with elements obtained from a user-defined function?
map()
7. What array method checks if all its elements satisfy a given condition?
every()
8. What array method checks if at least one of its elements satisfies a given condition?some()
9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
False
10. True or False: array.slice() copies elements from original array and returns them as a new array.
True

*/
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
let students = ["John", "Joe", "Jane", "Jessie"];
let students1 = ["John", "Joe", "Jane", "Jessie",9];
function addToEnd(students, name) {
  if (typeof name === "string") {
    students.push(name);
    return students
  } else {
    return "error - can only add strings to an array";
  }
}


// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

function addToStart(students, name) {
  if (typeof name === "string") {
    students.unshift(name);
    return students
  } else {
    return "error - can only add strings to an array";
  }
}


// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.


function elementChecker(students, value) {
  if (students.length === 0) {
    return "error - passed in array is empty";
  } else {
    return students.some(item => item === value);
  }
}

// 4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"if every element in the array ends in the passed in character, return true. Otherwise return false.Use the students array and the character "e" as arguments when testing.Tip - string characters may be accessed by passing in an index just like in arrays.

function checkAllStringsEnding(students, character) {
  if (students.length === 0) {
    return "error - array must NOT be empty";
  } else if (students.some(item => typeof item !== 'string')) {
    return "error - all array elements must be strings";
  } else if (typeof character !== 'string') {
    return "error - 2nd argument must be of data type string";
  } else if (character.length !== 1) {
    return "error - 2nd argument must be a single character";
  } else {
    character = character.toLowerCase(); // Convert to lowercase for case-insensitive comparison
    return students.every(item => item.charAt(item.length - 1).toLowerCase() === character);
  }
}

// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test..
function stringLengthSorter (students){
	if(students.some(item=> typeof item !== "string")){
		return "all array elements must be strings"
	} else {
		return students.sort((a,b) => a.length-b.length) 
	}

}


 // Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"return the number of elements in the array that start with the character argument, must be case-insensitiveUse the students array and the character "J" as arguments when testing.
function startsWithCounter(students, character) {
  if (students.length === 0) {
    return "error - array must NOT be empty";
  }
  if (students.some(item => typeof item !== "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof character !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (character.length !== 1) {
    return "error - 2nd argument must be a single character";
  } else {
    character = character.toLowerCase(); // Convert to lowercase for case-insensitive comparison
    let count = 0;
    for (let item of students) { // Use 'students' array instead of 'array'
      if (item.charAt(0).toLowerCase() === character) {
        count++;
      }
    }
    return count;
  }
}



 // Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitiveUse the students array and the string "jo" as arguments when testing.8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

// likeFinder function
function likeFinder(array, searchString) {
  if (array.length === 0) {
    return "error - array must NOT be empty";
  } else if (array.some(item => typeof item !== 'string')) {
    return "error - all array elements must be strings";
  } else if (typeof searchString !== 'string') {
    return "error - 2nd argument must be of data type string";
  } else {
    searchString = searchString.toLowerCase(); // Convert to lowercase for case-insensitive comparison
    return array.filter(item => item.toLowerCase().includes(searchString));
  }
}




// randomPicker function
function randomPicker(array) {
  if (array.length === 0) {
    return "error - array must NOT be empty";
  } else {
    let randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
  }
}
