class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;

    }

    archive() {
        if (this.isActive) {
            this.isActive = false;
        }
    }
        active(){
        if(this.isActive != true)
            this.isActive = true;
    }

    updatePrice(newPrice) {
        this.price = newPrice;
    }
}

class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity) {
        const cartItem = { product, quantity };
        this.contents.push(cartItem);
    }

    showCartContents() {
        console.log(this.contents);
    }

    updateProductQuantity(productName, newQuantity) {
        const cartItem = this.contents.find(item => item.product.name === productName);
        if (cartItem) {
            cartItem.quantity = newQuantity;
        }
    }

    clearCartContents() {
        this.contents = [];
        this.totalAmount = 0;
    }

    computeTotal() {
        this.totalAmount = this.contents.reduce((total, cartItem) => {
            return total + (cartItem.product.price * cartItem.quantity);
        }, 0);
    }
}

class Customer {
    constructor(name, email) {
        this.name = name;
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut() {
        if (this.cart.contents.length > 0) {
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.totalAmount
            });
            this.cart.clearCartContents();
        }
    }
}



const product1 = new Product("apple 1", 10);
const product2 = new Product("banana 2", 20);
const prodZ = new Product("soap", 9.99)

const customer1 = new Customer("roy", "jones");


const riza = new Customer("riza","riza@mail.com");




// const riza = new Customer("riza@mail.com");const prodZ = new Product("soap", 9.99)
// ;prodZ;prodZ.archive()riza.cart.addToCart(prodZ, 3);riza.cart.showCartContents();riza.cart.updateProductQuantity('soap', 5);riza.cart.computeTotal();riza.checkOut();riza.cart.clearCartContents();
// Ito po gamitin niyo sa browser
