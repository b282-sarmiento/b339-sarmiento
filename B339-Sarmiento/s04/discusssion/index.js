class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum / 4;
        return this;
    }

    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        if (this.passed) {
            this.passedWithHonors = this.gradeAve >= 90;
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}

class Section {
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined
        this.honorsPercentage=undefined;

    }

    addStudent(name, email, grades) {
        this.students.push(new Student(name, email, grades));
        return this;
    }
    // method for computing how many students in the section are honor sutdents 
    countHonorStudents() {
        let count = 0;

        this.students.forEach(student => {
            if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
        });

        this.honorStudents = count;
        return this;
    }
   computeHonorsPercentage() {
        this.honorsPercentage = (this.countHonorStudents().honorStudents / this.students.length) * 100;
        return this;
    }
}



// Create a section and add a student
const section1A = new Section('section1A');
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.students[0].computeAve()
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

