class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum / 4;
        return this;
    }

    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        if (this.passed) {
            this.passedWithHonors = this.gradeAve >= 90;
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}

class Section {
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined
        this.honorsPercentage=undefined;

    }

    addStudent(name, email, grades) {
        this.students.push(new Student(name, email, grades));
        return this;
    }
    // method for computing how many students in the section are honor sutdents 
    countHonorStudent() {
        let count = 0;

        this.students.forEach(student => {
            if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
        });

        this.honorStudents = count;
        return this;
    }
   computeHonorsPercentage() {
        this.honorsPercentage = (this.countHonorStudents().honorStudents / this.students.length) * 100;
        return this;
    }
}


class Grade {
    constructor(level) {
        this.level = level;
        this.sections = [];
        this.totalStudents = undefined;
        this.totalHonorStudents = undefined;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }

    addSection(name) {
        this.sections.push(new Section(name));
        return this;
    }

    countStudents() {
        let count = 0;
        this.sections.forEach(section => {
            count += section.students.length;
        });
        this.totalStudents = count;
        return this.totalStudents;
    }

    countHonorStudents() {
        let count = 0;
        this.sections.forEach(section => {
            count += section.countHonorStudent().honorStudents;
        });

        this.totalHonorStudents = count;
        return this.totalHonorStudents;
    }

    computeBatchAve() {
        let totalGradeSum = 0;
        let totalStudents = 0;

        this.sections.forEach(section => {
            section.students.forEach(student => {
                totalGradeSum += student.computeAve().gradeAve;
                totalStudents++;
            });
        });

        this.batchAveGrade = totalGradeSum / totalStudents;
        return this.batchAveGrade;
    }

    getBatchMinGrade() {
        let minGrade = Infinity;

        this.sections.forEach(section => {
            section.students.forEach(student => {
                student.computeAve();
                if (student.gradeAve < minGrade) {
                    minGrade = student.gradeAve;
                }
            });
        });

        this.batchMinGrade = minGrade;
        return this.batchMinGrade;
    }

    getBatchMaxGrade() {
        let maxGrade = -Infinity;

        this.sections.forEach(section => {
            section.students.forEach(student => {
                student.computeAve();
                if (student.gradeAve > maxGrade) {
                    maxGrade = student.gradeAve;
                }
            });
        });

        this.batchMaxGrade = maxGrade;
        return this.batchMaxGrade;
    }
}

grade1 = new Grade("grade1")

grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");



grade1.sections[0].addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
grade1.sections[0].addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
grade1.sections[0].addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
grade1.sections[0].addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

grade1.sections[1].addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
grade1.sections[1].addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
grade1.sections[1].addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
grade1.sections[1].addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

grade1.sections[2].addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
grade1.sections[2].addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
grade1.sections[2].addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
grade1.sections[2].addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

grade1.sections[3].addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
grade1.sections[3].addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
grade1.sections[3].addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
grade1.sections[3].addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);
