

class Student {
    constructor (name, email,grades){
        this.name = name;
        this.email = email;


        if(grades.length === 4) {
                    if(grades.every(grade => grade >= 0 && grade <= 100)) {
                        this.grades = grades
                    }
                } else {
                    this.grades = undefined;
                }
                this.gradeAve =undefined;
    }
    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

     logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

     listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;

    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum/4;
        return this
    }

    willPass() {
        return this.computeAve() >= 85 ? true : false;
    }

    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }


}
// Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);
