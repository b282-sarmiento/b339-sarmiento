
class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;

        if (grades.length === 4 && grades.every(grade => grade >= 0 && grade <= 100)) {
            this.grades = grades;
        } else {
            this.grades = undefined;
        }

        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        this.gradeAve = sum / 4;
        return this;
    }

    willPass() {
        this.passed = this.computeAve() >= 85;
        return this;
    }

    willPassWithHonors() {
        this.passedWithHonors = this.passed && this.computeAve() >= 90;
        return this;
    }
}

// Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

/*
1. Should class methods be included in the class constructor? No 
2. Can class methods be separated by commas?yes
3. Can we update an object’s properties via dot notation?yes
4. What do you call the methods used to regulate access to an object’s properties? getter and setter method
5. What does a method need to return in order for it to be chainable?
this
*/


