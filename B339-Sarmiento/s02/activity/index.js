/*1. What is the term given to unorganized code that's very hard to work with?spagetti codes

2. How are object literals written in JS? {}
3. What do you call the concept of organizing information and functionality to belong to an object?Object-Oriented Programming (OOP)
4. If the studentOne object has a method named enroll(), how would you invoke it?studentOne.enroll();
5. True or False: Objects can have objects as properties.true
6. What is the syntax in creating key-value pairs?key: value
7. True or False: A method can have no parameters and still work.true
8. True or False: Arrays can have objects as elements.true
9. True or False: Arrays are objects.true
10. True or False: Objects can have arrays as properties. true 
*/

// 1. Translate the other students from our boilerplate code into their own respective objects.

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

 // Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85



function login(email){
    console.log(`${email} has logged in`);
}



function logout(email){
    console.log(`${email} has logged out`);
}




function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}



let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],

  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grades: ${this.grades}`);
  },

  computeAve() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    return sum / this.grades.length;
  },

  willPass() {
    return this.computeAve() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeAve();
    if (average >= 90) {
      return true;
    } else if (average >= 85) {
      return false;
    } else {
      return undefined;
    }
  }
};

let studentTwo = {
	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],

	 login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grades: ${this.grades}`);
  },

  computeAve() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    return sum / this.grades.length;
  },

  willPass() {
    return this.computeAve() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeAve();
    if (average >= 90) {
      return true;
    } else if (average >= 85) {
      return false;
    } else {
      return undefined;
    }
  }
};




let studentThree = {
	name: "Jane",
	email: "jane@mail.com",
	grades:  [87, 89, 91, 93],

	 login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grades: ${this.grades}`);
  },

  computeAve() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    return sum / this.grades.length;
  },

  willPass() {
    return this.computeAve() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeAve();
    if (average >= 90) {
      return true;
    } else if (average >= 85) {
      return false;
    } else {
      return undefined;
    }
  }
};




let studentFour = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],

 login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grades: ${this.grades}`);
  },

  computeAve() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    return sum / this.grades.length;
  },

  willPass() {
    return this.computeAve() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeAve();
    if (average >= 90) {
      return true;
    } else if (average >= 85) {
      return false;
    } else {
      return undefined;
    }
  }
};




let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],

  countHonorStudents() {
    return this.students.filter(student => student.willPassWithHonors()).length;
  },

  honorsPercentage() {
    const honorCount = this.countHonorStudents();
    return (honorCount / this.students.length) * 100;
  },

  retrieveHonorStudentInfo() {
    return this.students
      .filter(student => student.willPassWithHonors())
      .map(student => ({ email: student.email, averageGrade: student.computeAve() }));
  },

  sortHonorStudentsByGradeDesc() {
    const honorStudents = this.retrieveHonorStudentInfo();
    return honorStudents.sort((a, b) => b.averageGrade - a.averageGrade);
  }
};
    